FROM golang:1.13 AS builder

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/gitlab.cern.ch/paas-tools/okd4-deployment/egroup-sync
COPY . ./
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix nocgo -mod vendor -o /main .



# As the oc binary is needed, use the origin-cli image
FROM quay.io/openshift/origin-cli:4.7

# Add the previously built app binary
COPY --from=builder /main  /

CMD ["/main"]
