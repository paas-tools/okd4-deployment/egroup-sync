# Overview
Application to synchronize OpenShift groups with LDAP and populate
members from an egroup.

The list of e-groups to sync is derived from groups referenced in namespace-scoped `RoleBinding` resources.
This allows site owners to just add a `Group` subject to their `RoleBinding`, and we automatically create an
Openshift `Group` synced with the corresponding e-group. We sync immediately as soon as we detect a new `Group`
reference in a `RoleBinding`; then we sync again every hour by default to propagate changes to the egroup.

The application has two components:

* A synchronous controller that listens to changes in RoleBindings and triggers e-group
expansion every time a user specifies a new group in a Rolebinding object.
The controller populates a `whitelist.txt` file at startup and then does a initial synchronization with
the existing groups. After that, it starts listening to changes in RoleBinding objects, adding
new occurrences to the `whitelist.txt` and resyncing all groups right after that.
Internal OpenShift groups like `system:...` (all non-internal groups are assumed to correspond to e-groups)
are excluded from the sync.

* A cronjob running every hour that expands already-synced groups in case their content
has changed. The `oc` CLI automatically reuses the list of groups from the last sync done
by the controller, it does not need the `whitelist.txt` file.

Groups are populated from LDAP using the `oc adm groups sync` command.

At the moment, recursive membership is *NOT* supported by this command.

If a group is specified in a RoleBinding but does not map to an existing e-group, nothing happens.

### Deployment

A Helm chart is provided in [`deploy/`](deploy/).
